<div style="background-color:rgb(245,129,30);" align="center"><img src="img/portada-myr.png" width="700px" alt="portada-myr"></div>

## Título del proyecto:

#### MyR_Android
------------------------------------------

#### Índice 📑
------------------------------------------
1. [Características 👨‍💻](#características-)
2. [Contenido del proyecto 📋](#contenido-del-proyecto-)
3. [Tecnologías 👾](#tecnologías-)
4. [IDE ⚙️](#ide-%EF%B8%8F)
5. [Instalación 🛠️](#instalación-%EF%B8%8F)
6. [Demo 📲](#demo-)
7. [Autor(es) ✒️](#autores-%EF%B8%8F)
8. [Institución Académica 🏫](#institución-académica-)
9. [Referencias 📖](#referencias-)

------------------------------------------
#### Características 👨‍💻
- Aplicación móvil realizada para el control de la venta de artículos y alimentos para mascotas.
- Proyecto con interfaz gráfica creada a través de [![Android Studio](https://img.shields.io/badge/-ANDROID%20STUDIO-3DDC84?logo=android%20studio&logoColor=white&style=for-the-badge)](https://es.wikipedia.org/wiki/Android_Studio).
- Disponible para Android 5.0 en adelante [![ANDROID](https://img.shields.io/badge/-ANDROID%205.0-3DDC84?logo=android&logoColor=white&style=for-the-badge)](https://www.android.com/intl/es_es/versions/lollipop-5-0/).
------------------------------------------
#### Contenido del proyecto 📋
|ARCHIVO|DESCRIPCIÓN|
|-----------|---------------|
|[Programa.java](app/src/main/java/com/example/programa/Programa.java)| Clase principal que contiene el modelo lógico de la aplicación, en dicha clase se crean varios métodos para hacer uso de la estructura de datos implementada para el manejo de los mismos. |
|[Producto.java](app/src/main/java/com/example/programa/Producto.java)|Esta clase. se usa para moldear los productos que agregamos, a objetos. Los cuales serán implementados en la estructura de datos para la modificación y control de los mismos.|
|[MainActivity.java](app/src/main/java/com/example/programa/MainActivity.java)| Esta clase, es el control principal del modelo y la vista de la aplicación, está ejecuta las acciones que realiza el usuario a través de la vista para llegar al modelo y entregar un resultado final.|
|[AndroidManifest.xml](app/src/main/AndroidManifest.xml)|Se encarga de conectar con el control principal y generar la ventana basada en portrait o land.|
|[activity_main.xml](app/src/main/res/layout/activity_main.xml) (portrait)|Ventana en modo (portrait) para smartphones Android, el contenido se verá verticalmente.|
|[activity_main.xml](app/src/main/res/layout-land/activity_main.xml) (land)|Ventana en modo (land) para smartphones Android, el contenido se verá horizontalmente.|
------------------------------------------
#### Tecnologías 👾
|TECNOLOGÍA|DESCRIPCIÓN|
|-----------|---------------|
|[![JAVA](https://img.shields.io/badge/-Java%208.2-007396?logo=java&logoColor=white&style=for-the-badge)](https://www.java.com/es/download/)<img src="https://image.flaticon.com/icons/png/512/226/226777.png" alt="Logo de java">|Java es un lenguaje de programación y una plataforma informática que fue comercializada por primera vez en 1995 por Sun Microsystems. Hay muchas aplicaciones y sitios web que no funcionarán, probablemente, a menos que tengan Java instalado y cada día se crean más. Java es rápido, seguro y fiable. Desde ordenadores portátiles hasta centros de datos, desde consolas para juegos hasta computadoras avanzadas, desde teléfonos móviles hasta Internet, Java está en todas partes, si es ejecutado en una plataforma no tiene que ser recompilado para correr en otra. Java es, a partir de 2012, uno de los lenguajes de programación más populares en uso, particularmente para aplicaciones de cliente-servidor de web, con unos diez millones de usuarios reportados.([Wikipedia, 2021](https://es.wikipedia.org/wiki/Java_(lenguaje_de_programaci%C3%B3n)))|
|[![Android Studio](https://img.shields.io/badge/-ANDROID%20STUDIO-3DDC84?logo=android%20studio&logoColor=white&style=for-the-badge)](https://developer.android.com/studio?hl=es)<img src="https://2.bp.blogspot.com/-tzm1twY_ENM/XlCRuI0ZkRI/AAAAAAAAOso/BmNOUANXWxwc5vwslNw3WpjrDlgs9PuwQCLcBGAsYHQ/s1600/pasted%2Bimage%2B0.png" height="100px" alt="imagen Android Studio">| Es un IDE, una interfaz de desarrollo. En realidad, es una especie de escritorio de trabajo para un desarrollador. Allí se encuentra nuestro proyecto, las carpetas del mismo, los archivos que hay en él, y todo lo necesario para acabar creando la aplicación. Lo mejor de Android Studio es que ha sido creado por Google y fue presentado hace tan solo unos meses, por lo que no hablamos de una herramienta antigua y nada depurada, sino de un programa muy moderno que encima ha sido creado por los mismos que han creado el sistema operativo.([Ecured,2014](https://www.ecured.cu/Android_Studio))|
|      [![XML](https://img.shields.io/badge/XML-orange)](https://es.wikipedia.org/wiki/Extensible_Markup_Language) <img src="https://cdn.iconscout.com/icon/free/png-512/xml-file-2330558-1950399.png" height="100px" alt="imagen xml logo">|XML es un lenguaje de metamarcado que ofrece un formato para la descripción de datos estructurados. Esto facilita unas declaraciones de contenido más precisas y unos resultados de búsquedas más significativos en varias plataformas. Además, XML habilitará una nueva generación de aplicaciones para ver y manipular datos basadas en el Web. Permiten la comunicación por el Web a través de muchas capas de tecnología interoperativa. Una capa importante es la presentación visual y la interfaz de usuario, ejemplificada en los estándares actuales, como HTML, GIF y JScript™. Dichos estándares permiten crear una página una sola vez y mostrarla en momentos distintos para varios receptores.([Euskalnet,2021](http://www.euskalnet.net/apetxebari/xml.htm))|


------------------------------------------
#### IDE ⚙️
###### El proyecto se desarrolla con las herramientas:
|   	NOMBRE|   	DESCRIPCIÓN|    ICONO|
|---	|---	|---   |
|   	[Android Studio](https://developer.android.com/studio) | 	Android Studio, es un entorno de desarrollo integrado oficial para la plataforma Android. ([Wikipedia, 2021](https://es.wikipedia.org/wiki/Android_Studio)) [Descargar](https://developer.android.com/studio?hl=es-419&gclid=CjwKCAjwg4-EBhBwEiwAzYAlsu7zeQnYg39sUmQFhFfu6IeA2NRufHdYAwbsUFkd5SzmuUT5zMn5ARoCT98QAvD_BwE&gclsrc=aw.ds)|<img src="https://2.bp.blogspot.com/-tzm1twY_ENM/XlCRuI0ZkRI/AAAAAAAAOso/BmNOUANXWxwc5vwslNw3WpjrDlgs9PuwQCLcBGAsYHQ/s1600/pasted%2Bimage%2B0.png" height="50px" alt="imagen Android Studio">| 
|[Java N-IDE](https://github.com/tranleduy2000/javaide)|  	 Java N-IDE es un proyecto que se generó con la idea de aprender Java. Desde un teléfono Android, podemos compilar y correr nuestro codigo por consola. [Descargar](https://play.google.com/store/apps/details?id=com.duy.compiler.javanide&hl=es_CO&gl=US)| <img src="https://play-lh.googleusercontent.com/WH_sgfQ4AmeK0_dCLUrrDUEbJ0MSPGjpVHzArUGk7d1DRG7citAa3rOk1pZYaT2_jiY" height="50px" alt="imagen java-N-IDE">|

------------------------------------------
#### Instalación 🛠️
Para la instalación de la aplicación Android, debemos contar con un smartphone  o emulador con dicho sistema operativo (**Android 5.0 o superior**).
> 1. Nos dirigimos al siguiente enlace para descargar el apk: [Descargar](https://gitlab.com/Yoner_Silva/myr_android/-/raw/master/app/release/app-release.apk)
> 2. En nuestro smartphone, entramos a configuración de aplicaciones y permitimos instalaciones de aplicaciones de terceros (pues esta apk no se encuentra en playstore).
> 3. Si el archivo que se descargó tiene la extensión (**app-release.apk**), podemos saltarnos hasta el paso 7. De lo contrario, siga los pasos a continuación. 
> 4. En nuestro smartphone, buscamos el archivo descargado (**app-release.zip**) en nuestro administrador de archivos de preferencia.
> 5. Seleccionamos el archivo descargado y lo descomprimimos.
> 6. Abrimos la carpeta que se descomprimió y buscamos el archivo (**app-release.apk**).
> 7. Lo seleccionamos, y procedemos a instalar la aplicación.
> 8. Abrimos la aplicación, y aceptamos los permisos que nos aparezcan.

Ya con eso podemos usar nuestra aplicación sin necesidad de tener conexión a internet.

------------------------------------------
#### Demo 📲
|Demo 1|Demo 2|
|-------- |--------|
|<img src="img/1.jpg" height="550px" alt="Demo1"/>|<img src="img/2.jpg" height="550px" alt="Demo2"/>|

------------------------------------------
#### Autores ✒️
Proyecto desarrollado por:
- Yoner Arbey Silva Leal (**yonerarbeysl@ufps.edu.co**)
- Carlos Eduardo Contreras Mendoza (**carloseduardocmen@ufps.efu.co**)

------------------------------------------
#### Institución Académica 🏫
Proyecto desarrollado independientemente enfocado a la materia de Programación Orientada a Objetos ll del [Programa de Ingeniería de Sistemas](<https://ingsistemas.cloud.ufps.edu.co/>) de la [Universidad Francisco de Paula Santander](<https://ww2.ufps.edu.co/>).


------------------------------------------
#### Referencias 📖
###### Android Studio. (2021, 28 de marzo). Wikipedia, La enciclopedia libre.desde https://es.wikipedia.org/w/index.php?title=Android_Studio&oldid=134342148.
###### Java (lenguaje de programación). (2021, 15 de abril). Wikipedia, La enciclopedia libre. desde https://es.wikipedia.org/w/index.php?title=Java_(lenguaje_de_programaci%C3%B3n)&oldid=134786123.
###### Android Studio. (2014, febrero 27). EcuRed, en https://www.ecured.cu/index.php?title=Android_Studio&oldid=2174386.
###### Xml. (2021, abril 25). Euskalnet, en http://www.euskalnet.net/apetxebari/xml.htm.
------------------------------------------
