package com.example.programa;

public class Producto {

  private byte numKilos;
  private double precioUnidad;
  private double iva;
  private final byte porcentajeGan = 20;
  
  public void Producto(){
    
  }


  public double precioVenta20yIVA() {
    double calculo = 0.0;
    double copyPU = this.precioUnidad;
    if(this.numKilos > 1){
      copyPU /= this.numKilos;
    }
    if(this.iva > 0){
      copyPU += (copyPU * this.iva) / 100;
    }
    calculo = (copyPU * porcentajeGan) / 100;
    return copyPU + calculo;
  }

  public double precioVentaIva(){
    double copyPU = this.precioUnidad;
    if(this.numKilos > 1){
      copyPU /= this.numKilos;
    }
    if(this.iva > 0){
      copyPU += (copyPU * this.iva) / 100;
    }
    return copyPU;
  }
  
  public void setPU(double pU){
    this.precioUnidad = pU;
  }
  
  public void setIva(double iva){
    this.iva = iva;
  }
  
  public void setNumKilos(byte num){
    this.numKilos = num;
  }
  
  @Override
  public String toString(){
    return "Precio Unidad: "+precioUnidad+"\n"+
    "Número de Kilos: "+numKilos+"\n"+
    "Iva: "+iva+"\n"+ "Porcentaje de Ganancia: "+porcentajeGan+"\n";
  }
}
