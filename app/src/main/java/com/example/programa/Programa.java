package com.example.programa;

import java.util.ArrayList;
import java.util.Iterator;



public class Programa {
  private ArrayList<Producto> productos = new ArrayList();
  
  
  public void Programa(){
  }
  
  public void aggProducto(Producto producto){
    productos.add(producto);
  }
  
  public Producto modProducto(byte num, Producto producto){
    return productos.set(num-1, producto);
  }

  public int cantidad(){
    return this.productos.size();
  }

  public boolean estaVacio(){
    return this.productos.isEmpty();
  }

  public void limpiar(){
    this.productos.clear();
  }
  
  public String calcularPrecios1(){
    String cadena = "**************PRECIO VENTA***************"+"\n"+"               IVA               |         IVA + MARGEN     "+"\n";
    Iterator<Producto> it = productos.iterator();
    Producto producto;
    byte cont = 1;
    while(it.hasNext()){
      producto = it.next();
      cadena += cont+")           "+ String.format("%.2f", producto.precioVentaIva())+"          |                 "+String.format("%.2f",producto.precioVenta20yIVA())+ "\n";
      cont++;
    }
    return cadena;
  }

  public String calcularPrecios2(){
    String cadena = "";
    Iterator<Producto> it = productos.iterator();
    Producto producto;
    byte cont = 1;
    while(it.hasNext()){
      producto = it.next();
      cadena += cont+") "+ producto.precioVentaIva()+ "\n";
      cont++;
    }
    return cadena;
  }

  public String ToString(){
    String cadena = "";
    Iterator<Producto> it = productos.iterator();
    Producto producto;
    byte cont = 1;
    while(it.hasNext()){
      producto = it.next();
      cadena += cont+") "+ producto.toString()+"\n";
      cont++;
    }
    return cadena;
  }


}
