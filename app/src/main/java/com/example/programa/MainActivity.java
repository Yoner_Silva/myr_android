package com.example.programa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity<AddClientActivity> extends AppCompatActivity {
    private Programa programa;
    private TextView textView;
    private TextView pU;
    private TextView numKilos;
    private TextView iva;
    private TextView cantProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        programa = new Programa();
        textView = findViewById(R.id.textView);
        textView.setMovementMethod(new ScrollingMovementMethod());
        this.pU = findViewById(R.id.editTextPrecioUnidad);
        this.numKilos = findViewById(R.id.editTexTNumKilos);
        this.iva = findViewById(R.id.editTextIVA);
        this.cantProductos =  findViewById(R.id.editTextCantProductos);
    }

    public void buttonActionEnviar(View view){
        EditText text1 = findViewById(R.id.editTextPrecioUnidad);
        EditText text2 = findViewById(R.id.editTexTNumKilos);
        EditText text3 = findViewById(R.id.editTextIVA);

        if(text1.getText().toString().isEmpty()){
            Toast.makeText(this, "La casilla Precio Unidad no pueden estar vacia.", Toast.LENGTH_SHORT).show();
        }else{
            byte numKilos = 0;
            double iva = 0;
            if(!text2.getText().toString().isEmpty()){
                numKilos = Byte.parseByte(text2.getText().toString());
            }
            if (!text3.getText().toString().isEmpty()){
                iva = Double.parseDouble(text3.getText().toString());
            }
            double pU = Double.parseDouble(text1.getText().toString());
            Producto producto = new Producto();
            producto.setPU(pU);
            producto.setNumKilos(numKilos);
            producto.setIva(iva);

            programa.aggProducto(producto);
            this.pU.setText("");
            this.numKilos.setText("");
            this.iva.setText("");
            this.cantProductos.setText(String.valueOf(this.programa.cantidad()));
        }
    }

    public void buttonActionProcesar(View view){
        if(programa.estaVacio()){
            Toast.makeText(this, "No hay productos, agreguelos primero.", Toast.LENGTH_SHORT).show();
        }else{
            textView.setText(programa.calcularPrecios1());
        }
    }


    public void buttonActionLimpiar(View view){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Limpiar");
        alertDialogBuilder
                .setMessage("Se borrará absolutamente todo..")
                .setCancelable(false)
                .setPositiveButton("Si",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        textView.setText("");
                        programa.limpiar();
                        cantProductos.setText("");
                        limpiarText();
                        Toast.makeText(MainActivity.this, "Limpiado con éxito.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                }).create().show();

    }

    private void limpiarText(){
        this.pU.setText("");
        this.iva.setText("");
        this.numKilos.setText("");
    }

    public void buttonActionMostrar(View view){
        if(programa.estaVacio()){
            Toast.makeText(this, "No hay productos, agreguelos primero.", Toast.LENGTH_SHORT).show();
        }else{
            textView.setText(programa.ToString());
        }
    }

    public void buttonActionModificar(View view){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Modificar Productos");
        final EditText input = new EditText(MainActivity.this);
        input.setInputType(1);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialogBuilder.setView(input);
                alertDialogBuilder
                .setMessage("Escriba el indice el cual desea modificar.")
                .setCancelable(false)
                .setPositiveButton("Si",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        byte indice = 0;
                        if(!input.getText().toString().isEmpty()) {
                            indice = Byte.parseByte(input.getText().toString());
                            EditText text1 = findViewById(R.id.editTextPrecioUnidad);
                            EditText text2 = findViewById(R.id.editTexTNumKilos);
                            EditText text3 = findViewById(R.id.editTextIVA);
                            if(text1.getText().toString().isEmpty()){
                                Toast.makeText(MainActivity.this, "Las casilla Precio Unidad no puede estar vacia.", Toast.LENGTH_SHORT).show();
                            }else {
                                byte numKilos = 0;
                                double iva = 0;
                                if (!text2.getText().toString().isEmpty()){
                                    numKilos = Byte.parseByte(text2.getText().toString());
                                }
                                if (!text3.getText().toString().isEmpty()){
                                    iva = Double.parseDouble(text3.getText().toString());
                                }
                                double pU = Double.parseDouble(text1.getText().toString());
                                Producto producto = new Producto();
                                producto.setPU(pU);
                                producto.setNumKilos(numKilos);
                                producto.setIva(iva);
                                programa.modProducto(indice,producto);
                                limpiarText();
                                textView.setText(programa.ToString());
                            }
                        }else{
                            Toast.makeText(MainActivity.this, "Escriba el índice primero.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    public void ocultarTeclado(View view){
        if(view != null){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
}